import React, { useState, createRef } from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { Container, Box, Card, CardHeader, CardContent, Button } from "@material-ui/core";

// core components
import HeaderSimple from "components/Headers/HeaderSimple.js";
import componentStyles from "assets/theme/views/admin/general.js";
// notifications
import NotificationAlert from "react-notification-alert";
import notificate from "variables/notificate.js";

const useStyles = makeStyles(componentStyles);

const StudentsCreate = (props) => {
  const classes = useStyles();
  const [student, setStudent] = useState({ name: '', lastName: '', dateBirth: '', 
    address: '', section: '' })
  const form = createRef()
  const notification = createRef()

  const changeHandler = (e) => {
    setStudent({...student,
      [e.target.name]: e.target.value})
  }

  const submitHandler = (e) => {
    e.preventDefault()
    console.log("submit")
    fetch('http://localhost:9000/students/insert', {//Ejecutar petición para registrar
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(student)
    })
      .then(res => res.text())
      .then(res => {
        if (res == "true") {
          notificate(notification, '¡Registro Exitoso!', `El Alumno ${student.name} ${student.lastName} ha sido registrado`, 'primary')
          setTimeout(() => {
            props.history.push('students')
          }, 800)
        }
        else {
          notificate(notification, '¡Error!', 'Ha ocurrido un error. Intente otra vez', 'danger')
        }
      })
      .catch(err => {
        notificate(notification, '¡Error!', 'Ha ocurrido un error. Intente otra vez', 'danger')
      })
  }

  return (
    <>
      <HeaderSimple />
      {/* Page content */}
      <Container
        maxWidth={false}
        component={Box}
        marginTop="-6rem"
        classes={{ root: classes.containerRoot }}
      >
        <Card classes={{ root: classes.cardRoot }}>
          <CardHeader
            className={classes.cardHeader}
            title="Registrar Alumno"
            titleTypographyProps={{
              component: Box,
              marginBottom: "0!important",
              variant: "h3",
            }}
          ></CardHeader>
          <CardContent>
            <form id="formStudentsCreate" ref={form} onSubmit={submitHandler}>
              <div className="row my-3">
                <div className="form-group col-md-6">
                  <label htmlFor="name"><b>Nombre</b></label>
                  <input type="text" className="form-control" id="name" name="name" pattern="[A-Za-z ÁÉÍÓÚáéíóú]{2,50}" required onChange={changeHandler} />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="lastName"><b>Apellido</b></label>
                  <input type="text" className="form-control" id="lastName" name="lastName" pattern="[A-Za-z ÁÉÍÓÚáéíóú]{2,50}" required onChange={changeHandler} />
                </div>
              </div>
              <div className="row my-3">
                <div className="form-group col-md-6">
                  <label htmlFor="dateBirth"><b>Fecha de Nacimiento</b></label>
                  <input type="date" className="form-control" id="dateBirth" name="dateBirth" required onChange={changeHandler} />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="address"><b>Dirección</b></label>
                  <textarea className="form-control" id="address" name="address" minLength="3" maxLength="255" required  onChange={changeHandler}></textarea>
                </div>
              </div>
              <div className="row my-3">
                <div className="form-group col-md-6">
                  <label htmlFor="section"><b>Sección</b></label>
                  <select className="form-select" id="section" name="section" onChange={changeHandler} required>
                    <option value=""></option>
                    <option value="HI1102">HI1102</option>
                    <option value="IN4301">IN4301</option>
                    <option value="AD3101">AD3101</option>
                  </select>
                </div>
              </div>
              <div className="row mt-3 d-flex justify-content-center">
                <div className="col-md-6 d-flex justify-content-center">
                  <Button type="submit" className="bg-primary text-white">Registrar</Button>
                </div>
              </div>

            </form>
          </CardContent>
        </Card>

      </Container>

      <NotificationAlert ref={notification} className="notificacion-alert" zIndex={1300} style={{zIndex:1300}} />
    </>
  );
};

export default StudentsCreate;
