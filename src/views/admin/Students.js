import React, { useState, useEffect, createRef, useContext } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import {
  Container, Box, Card, CardHeader, CardContent, Button, Modal
} from "@material-ui/core";
import MaterialTable from "components/Tables/MaterialTable.js";

// core components
import HeaderSimple from "components/Headers/HeaderSimple.js";
import componentStyles from "assets/theme/views/admin/general.js";
// notifications
import NotificationAlert from "react-notification-alert";
import notificate from "variables/notificate.js";
//Context
import { DataContext } from "components/Contexts/DataContext.js";

const useStyles = makeStyles(componentStyles);

const Students = (props) => {
  const classes = useStyles();
  const [tableLoading, setTableLoading] = useState(true)//Efecto cargando de la Tabla
  const [listUpdated, setListUpdated] = useState(false)
  const [students, setStudents] = useState([])//Estado para los registros de Alumnos
  const [studentDisable, setStudentDisable] = useState({})
  const [studentEnable, setStudentEnable] = useState({})
  const [modalDisable, setModalDisable] = useState(false)//State pare Modal
  const [modalEnable, setModalEnable] = useState(false)
  const {setData} = useContext(DataContext)
  const notification = createRef()
  
  const tableColumns = [//Columnas de la Tabla
    {
      title: 'Nombre',
      field: 'name',
    },
    {
      title: 'Apellido',
      field: 'lastName',
    },
    {
      title: 'Fecha de Nacimiento',
      field: 'dateBirthFormated',
    },
    {
      title: 'Sección',
      field: 'section',
    },
    {
      title: 'Acciones',
      align: 'center',
      field: 'status',
      render: (row) =>//Renderizando los botones 
        <div>
          <Button className="text-warning" size="small" onClick={() => getHandler(row)} title="Modificar"><i className='fa fa-edit'></i></Button>
          <Button size="small"
            title={row.status ? "Inhabilitar" : "Habilitar"}
            onClick={row.status ? () => confirmDisable(row) : () => confirmEnable(row)}
          >
            <i className={row.status ? "fa fa-check-circle text-success" : "fa fa-times text-secondary"}></i>
          </Button>
        </div>
      ,
    },
  ]
  const styleModalBox = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 'auto',
    maxWidth: '50%',
    padding: '2rem 1.5rem',
    bgcolor: 'background.paper',
    boxShadow: 24,
  };
  useEffect(() => {
    setTableLoading(true)
    fetch('http://localhost:9000/students/get')
      .then(res => res.json())
      .then(res => {
        setStudents(res)
        setTableLoading(false)
      })
      .catch(err => {
        notificate(notification, '¡Error!', 'Ha ocurrido un error al cargar los Alumnos', 'danger')
        setTableLoading(false)
        console.log(err)
      })
  }, [listUpdated])

  const getHandler = (element) => {//Agregar al context y redireccionar al componente actualizar
    setData({
      student: element
    })
    props.history.push('studentsUpdate')
  }
  const confirmDisable = (element) => {
    setStudentDisable(element)
    setModalDisable(true)
  }
  const confirmEnable = (element) => {
    setStudentEnable(element)
    setModalEnable(true)
  }
  const disableHandler = () => {//Inhabilitar Registro
    fetch('http://localhost:9000/students/disable/' + studentDisable.id, {//Ejecutar Inhabilitación
      method: 'POST',
    })
      .then(res => res.text())
      .then(res => {
        if (res == "true") {
          notificate(notification, '¡Inhabilitación Exitosa!', `El Alumno ${studentDisable.name} ${studentDisable.lastName} ha sido inhabilitado`, 'primary')
          setListUpdated(!listUpdated)
          setModalDisable(false)
        }
        else {
          notificate(notification, '¡Error!', 'Ha ocurrido un error. Intente otra vez', 'danger')
          console.log(res)
        }
      })
      .catch(err => {
        notificate(notification, '¡Error!', 'Ha ocurrido un error. Intente otra vez', 'danger')
        console.log(err)
      })
  }
  const enableHandler = () => {//Inhabilitar Registro
    fetch('http://localhost:9000/students/enable/' + studentEnable.id, {//Ejecutar Habilitación
      method: 'POST',
    })
      .then(res => res.text())
      .then(res => {
        if (res == "true") {
          notificate(notification, '¡Habilitación Exitosa!', `El Alumno ${studentEnable.name} ${studentEnable.lastName} ha sido habilitado`, 'primary')
          setListUpdated(!listUpdated)
          setModalEnable(false)
        }
        else {
          notificate(notification, '¡Error!', 'Ha ocurrido un error. Intente otra vez', 'danger')
          console.lo(res)
        }
      })
      .catch(err => {
        notificate(notification, '¡Error!', 'Ha ocurrido un error. Intente otra vez', 'danger')
        console.log(err)
      })
  }
  return (
    <>
      <HeaderSimple />
      {/* Page content */}
      <Container
        maxWidth={false}
        component={Box}
        marginTop="-6rem"
        classes={{ root: classes.containerRoot }}
      >
        <Card classes={{ root: classes.cardRoot }}>
          <CardHeader
            className={classes.cardHeader}
            title="Alumnos"
            titleTypographyProps={{
              component: Box,
              marginBottom: "0!important",
              variant: 'h3',
            }}
          ></CardHeader>
          <CardContent>
            <Button className="mb-2 bg-primary text-white" onClick={() => props.history.push('studentsCreate')}>Registrar Alumno</Button>
            <MaterialTable columns={tableColumns} data={students} loading={tableLoading} />
          </CardContent>
        </Card>

      </Container>

      <Modal
        open={modalDisable} onClose={() => { setModalDisable(false) }} style={{zIndex:1030}}
      >
        <Box sx={{ ...styleModalBox }}>
          <h2 className="text-center">¿Desea deshabilitar al Alumno?</h2>
          <Container className="mt-2 d-flex justify-content-center">
            <Button className="bg-primary text-white" onClick={disableHandler}>Sí</Button>
            <Button className="btn-secondary" onClick={() => { setModalDisable(false) }}>No</Button>
          </Container>
        </Box>
      </Modal>
      <Modal
        open={modalEnable} onClose={() => { setModalEnable(false) }} style={{zIndex:1030}}
      >
        <Box sx={{ ...styleModalBox }}>
          <h2 className="text-center">¿Desea habilitar al Alumno?</h2>
          <Container className="mt-2 d-flex justify-content-center">
            <Button className="bg-primary text-white" onClick={enableHandler}>Sí</Button>
            <Button onClick={() => { setModalEnable(false) }}>No</Button>
          </Container>
        </Box>
      </Modal>

      <NotificationAlert ref={notification} className="notificacion-alert" zIndex={1300} style={{zIndex:1300}} />
    </>
  );
};

export default Students;
