import { forwardRef } from 'react';
import MaterialTable from 'material-table';//Tabla 
import {
    AddBox, ArrowDownward,
    Check, ChevronLeft, ChevronRight, Clear, DeleteOutline, Edit, FilterList, FirstPage, LastPage, Remove,
    SaveAlt, Search, ViewColumn
} from '@material-ui/icons'; //Iconos Material UI

function MaterialTableData(props) {
    const { columns, data, loading } = props
    const pageSize = props.pageSize ? parseInt(props.pageSize) : 10
    const emptyDataSourceMessage = props.loading ? 'Cargando...' : 'No hay registros'
    const tableIcons = {//Iconos que usará la tabla
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    };
    return (
        <MaterialTable
            title=''
            isLoading={loading}
            columns={columns}
            data={data}
            icons={tableIcons}
            options={{
                loadingType: 'overlay',
                pageSizeOptions: [5, 10, 20, 30],
                pageSize
            }}
            localization={{
                body: {
                    emptyDataSourceMessage
                },
                toolbar: {
                    searchTooltip: 'Buscar',
                    searchPlaceholder: 'Buscar'
                },
                pagination: {
                    labelRowsSelect: 'Filas',
                    labelDisplayedRows: '{from}-{to} de {count}',
                    firstAriaLabel: 'Primera Página',
                    firstTooltip: 'Primera Página',
                    previousAriaLabel: 'Página Anterior',
                    previousTooltip: 'Página Anterior',
                    lastAriaLabel: 'Última Página',
                    lastTooltip: 'Última Página',
                    nextAriaLabel: 'Página Siguiente',
                    nextTooltip: 'Página Siguiente',
                }
            }}
        />
    )
}
export default MaterialTableData