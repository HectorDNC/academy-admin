const notificate = (notification, title = '', message = '', type = 'warning') => {//Mostrar Notificación
    var options = {};
    options = {
        place: 'bl',
        message: (
            <div>
                <div>
                    <b>{title}</b>
                </div>
                <div>
                    {message}
                </div>
            </div>
        ),
        type: type,
        // icon: "fa fa-info",
        zIndex: 1300,
        autoDismiss: 10
    }
    if (notification.current != null) {
        notification.current.notificationAlert(options);
    }
    else {
        console.log('notification null')
    }
}
export default notificate;